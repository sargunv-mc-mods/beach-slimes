package me.sargunvohra.mcmods.beachslimes;

import me.sargunvohra.mcmods.beachslimes.entity.BeachSlimeEntityRenderer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;

public class BeachSlimesClientInit implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.INSTANCE.register(
                BeachSlimesInit.entityType,
                ((manager, context) -> new BeachSlimeEntityRenderer(manager))
        );
    }
}
