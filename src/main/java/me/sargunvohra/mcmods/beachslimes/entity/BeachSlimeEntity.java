package me.sargunvohra.mcmods.beachslimes.entity;

import net.minecraft.entity.EntityData;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.mob.SlimeEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.particle.ParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Difficulty;
import net.minecraft.world.LocalDifficulty;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;

public class BeachSlimeEntity extends SlimeEntity {
    public BeachSlimeEntity(EntityType<BeachSlimeEntity> type, World world) {
        super(type, world);
    }

    @Override
    public EntityData initialize(
        WorldAccess world,
        LocalDifficulty difficulty,
        SpawnReason spawnReason,
        EntityData data,
        CompoundTag tag
    ) {
        EntityData ret = super.initialize(world, difficulty, spawnReason, data, tag);
        setSize(1 << (this.random.nextInt(10) < 1 ? 1 : 0), true);
        return ret;
    }

    @Override
    public boolean canSpawn(WorldAccess world, SpawnReason spawnReason) {
        // seed for testing: 6211790508050301845
        BlockPos down = getBlockPos().down();
        boolean ret = !world.getDifficulty().equals(Difficulty.PEACEFUL);
        ret = ret && this.getY() >= 50f && this.getY() <= 70f;
        ret = ret && world.getBiome(down).getCategory() == Biome.Category.BEACH;
        // this last condition is basically super.super.canSpawn()
        ret = ret && (spawnReason == SpawnReason.SPAWNER || world.getBlockState(down).allowsSpawning(world, down, getType()));
        return ret;
    }

    @Override
    protected ParticleEffect getParticles() {
        return ParticleTypes.SPLASH;
    }
}
