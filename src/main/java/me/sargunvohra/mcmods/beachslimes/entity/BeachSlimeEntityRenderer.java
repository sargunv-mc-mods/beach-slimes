package me.sargunvohra.mcmods.beachslimes.entity;

import me.sargunvohra.mcmods.beachslimes.Utils;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.SlimeEntityRenderer;
import net.minecraft.entity.mob.SlimeEntity;
import net.minecraft.util.Identifier;

public class BeachSlimeEntityRenderer extends SlimeEntityRenderer {
    public BeachSlimeEntityRenderer(EntityRenderDispatcher dispatcher) {
        super(dispatcher);
    }

    private final Identifier skin = Utils.id("textures/entity/slime/beachslime.png");

    @Override
    public Identifier getTexture(SlimeEntity ent) {
        return skin;
    }
}
