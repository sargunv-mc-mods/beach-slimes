package me.sargunvohra.mcmods.beachslimes.mixin;

import me.sargunvohra.mcmods.autoconfig1u.AutoConfig;
import me.sargunvohra.mcmods.autoconfig1u.serializer.Toml4jConfigSerializer;
import me.sargunvohra.mcmods.beachslimes.BeachSlimesInit;
import me.sargunvohra.mcmods.beachslimes.ModConfig;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.world.biome.BeachBiome;
import net.minecraft.world.biome.Biome;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BeachBiome.class)
public class BeachBiomeMixin extends Biome {
    protected BeachBiomeMixin(Settings settings) {
        super(settings);
        throw new IllegalStateException();
    }

    @Inject(method = "<init>*", at = @At("RETURN"))
    private void onConstruct(CallbackInfo ci) {
        ModConfig config = AutoConfig.register(ModConfig.class, Toml4jConfigSerializer::new).getConfig();
        super.addSpawn(
                SpawnGroup.MONSTER,
                new Biome.SpawnEntry(
                        BeachSlimesInit.entityType,
                        config.spawnWeight,
                        config.spawnMinGroupSize,
                        config.spawnMaxGroupSize
                )
        );
    }
}
