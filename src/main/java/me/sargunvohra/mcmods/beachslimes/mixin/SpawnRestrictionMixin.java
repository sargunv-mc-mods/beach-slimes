package me.sargunvohra.mcmods.beachslimes.mixin;

import me.sargunvohra.mcmods.beachslimes.BeachSlimesInit;
import me.sargunvohra.mcmods.beachslimes.Utils;
import me.sargunvohra.mcmods.beachslimes.entity.BeachSlimeEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnRestriction;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.world.Heightmap;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Map;

@Mixin(SpawnRestriction.class)
public abstract class SpawnRestrictionMixin {
    @Shadow
    @Final
    private static Map<EntityType<?>, Object /* SpawnRestriction.Entry */> RESTRICTIONS;

    @Inject(method = "register", at = @At("HEAD"))
    private static void register(
        EntityType<?> type,
        SpawnRestriction.Location location,
        Heightmap.Type heightMapType,
        SpawnRestriction.SpawnPredicate<?> canSpawn,
        CallbackInfo ci
    ) {
        if (type == EntityType.SLIME) {
            for (Class<?> cls : SpawnRestriction.class.getDeclaredClasses()) {
                if (cls.isEnum() || cls.isInterface()) continue;
                RESTRICTIONS.put(
                    BeachSlimesInit.entityType,
                    Utils.construct(
                        cls,
                        heightMapType,
                        location,
                        (SpawnRestriction.SpawnPredicate<BeachSlimeEntity>) MobEntity::canMobSpawn
                    )
                );
                break;
            }
        }
    }
}
