package me.sargunvohra.mcmods.beachslimes;

import me.sargunvohra.mcmods.autoconfig1u.ConfigData;
import me.sargunvohra.mcmods.autoconfig1u.annotation.Config;
import me.sargunvohra.mcmods.autoconfig1u.annotation.ConfigEntry;

@Config(name = "beachslimes")
@Config.Gui.Background("beachslimes:icon.png")
public class ModConfig implements ConfigData {
    @ConfigEntry.Gui.PrefixText
    public final int spawnWeight = 100;
    public final int spawnMaxGroupSize = 3;
    public final int spawnMinGroupSize = 3;
}
