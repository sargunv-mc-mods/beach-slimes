package me.sargunvohra.mcmods.beachslimes;

import me.sargunvohra.mcmods.beachslimes.entity.BeachSlimeEntity;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.registry.Registry;

public class BeachSlimesInit implements ModInitializer {

    public static final EntityType<BeachSlimeEntity> entityType = FabricEntityTypeBuilder.
            create(SpawnGroup.MONSTER, BeachSlimeEntity::new)
            .dimensions(EntityDimensions.changing(2.04f, 2.04f))
            .build();

    @Override
    public void onInitialize() {
        FabricDefaultAttributeRegistry.register(entityType, HostileEntity.createHostileAttributes());
        Registry.register(Registry.ENTITY_TYPE, Utils.id("beach_slime"), entityType);
        Registry.register(
                Registry.ITEM,
                Utils.id("beach_slime_spawn_egg"),
                new SpawnEggItem(entityType, 0x513EA0, 0x7E6EBF, new Item.Settings().group(ItemGroup.MISC))
        );
    }
}
