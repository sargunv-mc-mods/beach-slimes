# Beach Slimes

Bouncy prey for sticky piston lovers.

## Information

Check out this mod on [CurseForge][].

## Building from source

```bash
git clone https://gitlab.com/sargunv-mc-mods/beach-slimes.git
cd beach-slimes
./gradlew build
# On Windows, use "gradlew.bat" instead of "gradlew"
```

[CurseForge]: https://minecraft.curseforge.com/projects/beach-slimes
